import { NgModule } from '@angular/core';
import { BadgeWidgetComponent } from './badge-widget.component';
import { BadgeModule } from '../badge/badge.module';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [BadgeModule, CommonModule],
  exports: [BadgeWidgetComponent],
  declarations: [BadgeWidgetComponent],
  providers: [],
})
export class BadgeWidgetModule {}
