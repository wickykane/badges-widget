import { Badge } from '../../../types/badge';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-unlock-badge',
  templateUrl: './unlock-badge.component.html',
  styleUrl: './unlock-badge.component.scss',
})
export class UnlockBadgeComponent {
  @Input() badge: Badge | null = null;
  @Input() visible: boolean = false;
}
