import { NgModule } from '@angular/core';
import { UnlockBadgeComponent } from './unlock-badge.component';
import { DialogModule } from 'primeng/dialog';

@NgModule({
  imports: [DialogModule],
  exports: [UnlockBadgeComponent],
  declarations: [UnlockBadgeComponent],
  providers: [],
})
export class UnlockBadgeModule {}
