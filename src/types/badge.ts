export interface Badge {
  icon: string;
  description: string;
  percentage?: number;
}
