import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { UnlockBadgeModule } from './components/unlock-badge/unlock-badge.module';
import { BadgeWidgetModule } from './components/badge-widget/badge-widget.module';

import { Badge } from '../types';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, BadgeWidgetModule, UnlockBadgeModule],
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  _timeToDisplayUnlock = 5000;

  public showUnlockModal: boolean = false;
  public badgeList: Badge[] = [
    {
      description: 'Rank silver',
      icon: 'assets/badges/badge-1.webp',
      percentage: 100,
    },
    {
      description: 'Rank Gold',
      icon: 'assets/badges/badge-2.webp',
      percentage: 50,
    },
    {
      description: 'Rank Diamond',
      icon: 'assets/badges/badge-3.webp',
      percentage: 0,
    },
    {
      description: 'Rank Platium',
      icon: 'assets/badges/badge-4.webp',
      percentage: 0,
    },
  ];

  ngOnInit(): void {
    setTimeout(() => {
      this.showUnlockModal = true;
    }, this._timeToDisplayUnlock);
  }
}
