import { Badge } from '../../../types/badge';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-badge-widget',
  templateUrl: './badge-widget.component.html',
  styleUrl: './badge-widget.component.scss',
})
export class BadgeWidgetComponent {
  @Input() badges: Badge[] = [];
}
