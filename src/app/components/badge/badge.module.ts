import { NgModule } from '@angular/core';
import { ProgressBarModule } from 'primeng/progressbar';
import { BadgeComponent } from './badge.component';
import { TooltipModule } from 'primeng/tooltip';

@NgModule({
  imports: [ProgressBarModule, TooltipModule],
  exports: [BadgeComponent],
  declarations: [BadgeComponent],
  providers: [],
})
export class BadgeModule {}
