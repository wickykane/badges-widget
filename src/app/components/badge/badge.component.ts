import { Badge } from './../../../types/badge';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-badge',
  templateUrl: './badge.component.html',
})
export class BadgeComponent {
@Input() data: Badge | null =  null;
}
